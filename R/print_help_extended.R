#' miscHelperFunctions
#'
#' Prints usage file to stderr or stdout (user specified)
#' @param optparser OptionParser object created using the optparse package
#' @param out.stderr Should we print to stderr. Defaults to true. If false, will print to stdout
#'
#' @return NULL
#' @export
#'
#' @examples
#' library(optparse)
#'
#' option_list <- list(
#' make_option(c("-u", "--upstream"), action="store", default=499, type = "integer",
#'            dest="bases_upstream.i", help="Bases upstream to extend by [%default]"),
#' make_option(c("-d", "--downstream"), action="store",default=100, type = "integer",
#'            dest="bases_downstream.i", help="Bases downstream to extend by [%default]"),
#' make_option(c("-i", "--input"), type="character", default="stdin",
#'            help="Number of random normals to generate [%default]")
#' )
#' optparser <- OptionParser(option_list=option_list)
#' opt = parse_args(optparser)
#'
#' #Print usage details to stderr rather than stdout
#' print_help_extended(optparser = optparser, out.stderr = TRUE)

print_help_extended <- function(optparser, out.stderr = TRUE){
  if ("optparse" %in% loadedNamespaces() == F) { stop("print_help_extended requires the optparse package") }
  if (class(optparser) != "OptionParser") { stop("optparser must be of the class: OptionParser")}
  if (out.stderr){
    sink(file = stderr(), type = "output")
    optparse::print_help(optparser)
    sink(type = "message")
  } else
    optparse::print_help(optparser)
}
